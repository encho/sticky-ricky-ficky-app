# Birthday site for pianist

[<img src="https://s3.eu-central-1.amazonaws.com/thetellys/2018-06-06_ricky-bday-site.png">](https://happy-birthday-ricky.now.sh/)


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>

## Deployment

    now

and then

     now alias <DEPLOY_URL> happy-birthday-ricky.now.sh
