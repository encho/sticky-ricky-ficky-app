import * as React from "react";
import styled, { keyframes, css } from "styled-components";

const bgColorAnimation = keyframes`
  0% {
    background-color: #222222;
    color: #2c2c2c;
  }
  50% {
    background-color: #333333;
    color: #3c3c3c;
  }
  100% {
    background-color: #222222;
    color: #2c2c2c;
  }
`;

const getRandomInt = (min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

const names = ["Ricky", "Ricky", "Ficky", "Sticky"];

const getRandomDelay = () => `${getRandomInt(0, 200) / 200}s`;
const getRandomText = () => names[Math.floor(Math.random() * names.length)];

const animation = () =>
  css`
    animation: ${bgColorAnimation} 2.5s infinite;
    animation-delay: ${getRandomDelay()};
  `;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.4rem;
  font-family: "Righteous", cursive;
  text-transform: uppercase;
  padding: 1rem 0;
  background-color: #222222;
  border: 0px solid transparent;
  color: #333333;
  ${animation};
`;

const StickyTile = () => <Wrapper>{getRandomText()}</Wrapper>;

export default StickyTile;
