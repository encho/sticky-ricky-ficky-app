import * as React from "react";
import { Flex, Box } from "grid-styled";
import styled, { injectGlobal } from "styled-components";
import WelcomeText from "./WelcomeText";
import StickyTile from "./StickyTile";

const rangeArray = [...Array(150).keys()];

const Absolute = styled.div`
  position: fixed;
`;

const Gratulation = styled.div`
  width: 100vw;
  height: 100vh;
`;

injectGlobal`
  body {
    background: #222222;
  }
`;

const App = () => (
  <div>
    <Absolute>
      <Flex flexWrap>
        {rangeArray.map(n => (
          <Box width={[1 / 3, 1 / 3, 1 / 4, 1 / 6]} key={n}>
            {" "}
            <StickyTile />
          </Box>
        ))}
      </Flex>
    </Absolute>
    <Absolute>
      <Gratulation>
        <Flex
          style={{ height: "100%" }}
          flexDirection="column"
          justifyContent="space-around"
        >
          <div style={{ marginTop: "-4rem" }}>
            <WelcomeText />
            <Flex justifyContent="center">
              <Box width={[0.9, 0.62, 1 / 2]}>
                <iframe
                  width="100%"
                  height="166"
                  scrolling="no"
                  frameborder="no"
                  allow="autoplay"
                  src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/249461687&color=%23f05122&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"
                />
              </Box>
            </Flex>
          </div>
        </Flex>
      </Gratulation>
    </Absolute>
  </div>
);

export default App;
