import React, { Component } from "react";
import styled from "styled-components";
import { Heading as BaseText } from "rebass";

const Heading = styled(BaseText)`
  font-family: "Righteous", cursive;
  color: #ffffff;
`;

const WelcomeTextWrapper = styled.div`
  text-align: center;
  margin-bottom: 1.5rem;
`;

const WelcomeText = () => (
  <WelcomeTextWrapper>
    <Heading fontSize={[6, 7, 8]}>Thelly Jut.</Heading>
    <Heading fontSize={[6, 7, 8]}>Allet Jut.</Heading>
  </WelcomeTextWrapper>
);

export default WelcomeText;
